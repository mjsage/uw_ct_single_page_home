<?php
/**
 * @file
 * uw_ct_single_page_home.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_ct_single_page_home_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create uw_ct_single_page_home content'.
  $permissions['create uw_ct_single_page_home content'] = array(
    'name' => 'create uw_ct_single_page_home content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any uw_ct_single_page_home content'.
  $permissions['delete any uw_ct_single_page_home content'] = array(
    'name' => 'delete any uw_ct_single_page_home content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own uw_ct_single_page_home content'.
  $permissions['delete own uw_ct_single_page_home content'] = array(
    'name' => 'delete own uw_ct_single_page_home content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete terms in stem_event_types'.
  $permissions['delete terms in stem_event_types'] = array(
    'name' => 'delete terms in stem_event_types',
    'roles' => array(),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit any uw_ct_single_page_home content'.
  $permissions['edit any uw_ct_single_page_home content'] = array(
    'name' => 'edit any uw_ct_single_page_home content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own uw_ct_single_page_home content'.
  $permissions['edit own uw_ct_single_page_home content'] = array(
    'name' => 'edit own uw_ct_single_page_home content',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit terms in stem_event_types'.
  $permissions['edit terms in stem_event_types'] = array(
    'name' => 'edit terms in stem_event_types',
    'roles' => array(),
    'module' => 'taxonomy',
  );

  // Exported permission: 'enter uw_ct_single_page_home revision log entry'.
  $permissions['enter uw_ct_single_page_home revision log entry'] = array(
    'name' => 'enter uw_ct_single_page_home revision log entry',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_ct_single_page_home authored by option'.
  $permissions['override uw_ct_single_page_home authored by option'] = array(
    'name' => 'override uw_ct_single_page_home authored by option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_ct_single_page_home authored on option'.
  $permissions['override uw_ct_single_page_home authored on option'] = array(
    'name' => 'override uw_ct_single_page_home authored on option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_ct_single_page_home promote to front page option'.
  $permissions['override uw_ct_single_page_home promote to front page option'] = array(
    'name' => 'override uw_ct_single_page_home promote to front page option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_ct_single_page_home published option'.
  $permissions['override uw_ct_single_page_home published option'] = array(
    'name' => 'override uw_ct_single_page_home published option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_ct_single_page_home revision option'.
  $permissions['override uw_ct_single_page_home revision option'] = array(
    'name' => 'override uw_ct_single_page_home revision option',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_ct_single_page_home sticky option'.
  $permissions['override uw_ct_single_page_home sticky option'] = array(
    'name' => 'override uw_ct_single_page_home sticky option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'search uw_ct_single_page_home content'.
  $permissions['search uw_ct_single_page_home content'] = array(
    'name' => 'search uw_ct_single_page_home content',
    'roles' => array(),
    'module' => 'search_config',
  );

  // Exported permission: 'use text format uw_tf_sph_caption'.
  $permissions['use text format uw_tf_sph_caption'] = array(
    'name' => 'use text format uw_tf_sph_caption',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'filter',
  );

  // Exported permission: 'use text format uw_tf_sph_marketing_item'.
  $permissions['use text format uw_tf_sph_marketing_item'] = array(
    'name' => 'use text format uw_tf_sph_marketing_item',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'filter',
  );

  return $permissions;
}
