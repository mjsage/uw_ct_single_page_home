<?php
/**
 * @file
 * uw_ct_single_page_home.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_ct_single_page_home_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_image_default_styles().
 */
function uw_ct_single_page_home_image_default_styles() {
  $styles = array();

  // Exported image style: banner_large.
  $styles['banner_large'] = array(
    'label' => 'banner_large',
    'effects' => array(
      36 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 1000,
          'height' => 600,
        ),
        'weight' => 2,
      ),
      0 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'height' => 600,
          'width' => 1000,
        ),
        'weight' => 2,
      ),
    ),
  );

  // Exported image style: banner_med.
  $styles['banner_med'] = array(
    'label' => 'banner_med',
    'effects' => array(
      1 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 800,
          'height' => 400,
        ),
        'weight' => 2,
      ),
    ),
  );

  // Exported image style: banner_small.
  $styles['banner_small'] = array(
    'label' => 'banner_small',
    'effects' => array(
      2 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 480,
          'height' => 480,
        ),
        'weight' => 2,
      ),
    ),
  );

  // Exported image style: banner_xl.
  $styles['banner_xl'] = array(
    'label' => 'banner_xl',
    'effects' => array(
      5 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 1600,
          'height' => 553,
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function uw_ct_single_page_home_node_info() {
  $items = array(
    'uw_ct_single_page_home' => array(
      'name' => t('Single page home'),
      'base' => 'node_content',
      'description' => t('Edit the home page of your single page site.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => t('Edit the home page of your single page site.'),
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}

/**
 * Implements hook_paragraphs_info().
 */
function uw_ct_single_page_home_paragraphs_info() {
  $items = array(
    'copy_block' => array(
      'name' => 'Copy block',
      'bundle' => 'copy_block',
      'locked' => '1',
    ),
    'sph_generic_remote_events' => array(
      'name' => 'Remote events',
      'bundle' => 'sph_generic_remote_events',
      'locked' => '1',
    ),
    'sph_image_block' => array(
      'name' => 'Image block',
      'bundle' => 'sph_image_block',
      'locked' => '1',
    ),
    'sph_marketing_block' => array(
      'name' => 'Marketing block',
      'bundle' => 'sph_marketing_block',
      'locked' => '1',
    ),
    'sph_marketing_item' => array(
      'name' => 'Marketing item',
      'bundle' => 'sph_marketing_item',
      'locked' => '1',
    ),
    'sph_quicklinks_block' => array(
      'name' => 'Quicklinks block',
      'bundle' => 'sph_quicklinks_block',
      'locked' => '1',
    ),
    'sph_remote_events' => array(
      'name' => 'Remote Girls in STEM events',
      'bundle' => 'sph_remote_events',
      'locked' => '1',
    ),
    'sph_remote_events_block' => array(
      'name' => 'Remote events block',
      'bundle' => 'sph_remote_events_block',
      'locked' => '1',
    ),
    'uw_para_link_block' => array(
      'name' => 'Options',
      'bundle' => 'uw_para_link_block',
      'locked' => '1',
    ),
    'uw_para_options_blocks' => array(
      'name' => 'Options blocks',
      'bundle' => 'uw_para_options_blocks',
      'locked' => '1',
    ),
    'uw_para_quicklinks' => array(
      'name' => 'Quicklinks',
      'bundle' => 'uw_para_quicklinks',
      'locked' => '1',
    ),
  );
  return $items;
}

/**
 * Implements hook_rdf_default_mappings().
 */
function uw_ct_single_page_home_rdf_default_mappings() {
  $schemaorg = array();

  // Exported RDF mapping: uw_ct_single_page_home
  $schemaorg['node']['uw_ct_single_page_home'] = array(
    'rdftype' => array(
      0 => 'sioc:Item',
      1 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  );

  // Exported RDF mapping: stem_event_types
  $schemaorg['taxonomy_term']['stem_event_types'] = array(
    'rdftype' => array(
      0 => 'skos:Concept',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'rdfs:label',
        1 => 'skos:prefLabel',
      ),
    ),
    'description' => array(
      'predicates' => array(
        0 => 'skos:definition',
      ),
    ),
    'vid' => array(
      'predicates' => array(
        0 => 'skos:inScheme',
      ),
      'type' => 'rel',
    ),
    'parent' => array(
      'predicates' => array(
        0 => 'skos:broader',
      ),
      'type' => 'rel',
    ),
  );

  return $schemaorg;
}
