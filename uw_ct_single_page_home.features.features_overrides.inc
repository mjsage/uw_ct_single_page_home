<?php
/**
 * @file
 * uw_ct_single_page_home.features.features_overrides.inc
 */

/**
 * Implements hook_features_override_default_overrides().
 */
function uw_ct_single_page_home_features_override_default_overrides() {
  // This code is only used for UI in features. Exported alters hooks do the magic.
  $overrides = array();

  // Exported overrides for: variable
  $overrides["variable.page_title_default.value"] = '[site:name] | University of Waterloo';

 return $overrides;
}
