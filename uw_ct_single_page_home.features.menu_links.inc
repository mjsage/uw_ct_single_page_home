<?php
/**
 * @file
 * uw_ct_single_page_home.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function uw_ct_single_page_home_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-site-manager-vocabularies_stem-event-theme:admin/structure/taxonomy/stem_event_types.
  $menu_links['menu-site-manager-vocabularies_stem-event-theme:admin/structure/taxonomy/stem_event_types'] = array(
    'menu_name' => 'menu-site-manager-vocabularies',
    'link_path' => 'admin/structure/taxonomy/stem_event_types',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => 'STEM event theme',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-manager-vocabularies_stem-event-theme:admin/structure/taxonomy/stem_event_types',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 1,
    'language' => 'und',
    'menu_links_customized' => 1,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('STEM event theme');

  return $menu_links;
}
