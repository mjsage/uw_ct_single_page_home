// Register a template definition set named "default".
CKEDITOR.addTemplates( 'default',
{
	// Template definitions.
	templates :
		[
            {
                title: 'Block Quotes',
                description: 'Use with caution; improper use will cause usability issues.',
                html:
                    '<blockquote>' +
                    '<p>We should never forget how lucky we were to have men like Professor Tutte in our darkest hour and the extent to which their work not only helped protect Britain itself but [saved] countless lives.</p>' +
                    '<footer>' +
                    '<cite>' +
                    '<p>&#x2014; <strong>David Cameron</strong>, British Prime Minister, in a 2012 letter to Tutte’s remaining family in Newmarket, England</p>' +
                    '</cite>' +
                    '</footer>' +
                    '</blockquote>'
            }
			
		]
});
