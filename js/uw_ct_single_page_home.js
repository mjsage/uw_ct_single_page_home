/**
 * @file
 */

(function ($) {

  Drupal.behaviors.uw_ct_single_page_home = {

    attach: function (context, settings) {
      // If on single page home edit, move help text to below block title.
      if ($('#field-sph-blocks-add-more-wrapper > .form-item > .description').length) {
        $('#field-sph-blocks-add-more-wrapper > .form-item > .description').insertBefore($('#edit-field-sph-blocks-und-add-more-type'));
      }
      // On the load of the page or when adding a paragraph, ensure that only the URL appears.
      $.each($(".field-name-field-event-url input"), function(i, url) {
        // If the URL is null or blank, hide it.
        if ($(url).val() == "" || $(url).val() == null) {
          $container = $(url).closest('td');
          $title = $container.find("div[id*='field-event-title']");
          var fieldset = $container.find('fieldset');
          
          $title.hide();
          $(fieldset[1]).hide();
        }
      });

      $.each($(".field-name-field-generic-event-url input"), function(i, url) {
        // If the URL is null or blank, hide it.
        if ($(url).val() == "" || $(url).val() == null) {
          $container = $(url).closest('td');
          $title = $container.find("div[id*='field-generic-event-title']");
          var fieldset = $container.find('fieldset');
          
          $title.hide();
          $(fieldset[1]).hide();
        }
      });

      // Add a listener to each input or a Remote Event URL.
      $('.field-name-field-event-url input').live('input', function() {
        $input = $(this);
        var url = $input.val();
        var matches = url.match(/http(.*)\:\/\/(.*)uwaterloo\.ca\/(.*)\/events\/(.*)/);

        // Once a match of https://uwaterloo.ca/<site_url>/event/, load in the data.
        if (matches.length > 0) {
          // Split the URL apary to get data from API
          var url_parts = $input.val().split("/");
          var site = url_parts[3];

          // Get data from API using site_url.
          $.getJSON("https://api.uwaterloo.ca/v2/events/" + site + ".json?key=dem0hash", function(json) {
            // Step through each event
            $.each(json.data, function(i, event){
              // Set teh container to the earliest td and the title to the title div and fieldset to all the fieldsets.
              $container = $input.closest('td');
              $title = $container.find("div[id*='field-event-title']");
              var fieldset = $container.find('fieldset');

              // If the event and the URL entered are the same, then add date/time, title and cost.
              if (event.link == $input.val()) {
                // Make call to API based on the event_id.
                $.getJSON("https://api.uwaterloo.ca/v2/events/" + site + "/" + event.id + ".json?key=dem0hash", function(event_details){
                  // Step through each date/time and add it to the appropriate places on the page.
                  $.each(event_details.data.times, function(j, event_time_date) {
                    // Check if there is a start date.
                    if (event_time_date.start.length > 0) {
                      var start_time_hours, start_time_mins, start_time_suffix;
                      var end_time_hours, end_time_mins, end_time_suffix;
                      var start_time_present = false;
                      var end_time_present = false;

                      // Check if there is a start time.
                      if (event_time_date.start_time.length > 0) {
                        start_time_present = true;

                        // Get hours and minutes.
                        start_time_hours = event_time_date.start_time.substring(0,2);
                        start_time_mins = event_time_date.start_time.substring(3,5);

                        // Convert 24 hour to 12 hour.
                        start_time_suffix = (start_time_hours >= 12)? 'pm' : 'am';
                        start_time_hours = (start_time_hours > 12)? start_time_hours -12 : start_time_hours;
                        start_time_hours = (start_time_hours == '00')? 12 : start_time_hours;
                      }

                      // Check if there is an end time.
                      if (event_time_date.end_time.length > 0) {
                        end_time_present = true;

                        // Get hours and minutes.
                        end_time_hours = event_time_date.end_time.substring(0,2);
                        end_time_mins = event_time_date.end_time.substring(3,5);

                        // Convert 24 hour to 12 hour.
                        end_time_suffix = (end_time_hours >= 12)? 'pm' : 'am';
                        end_time_hours = (end_time_hours > 12)? end_time_hours -12 : end_time_hours;
                        end_time_hours = (end_time_hours == '00')? 12 : end_time_hours;
                      }

                      // Insert start time into the page.
                      $(fieldset[1]).find("input[id*='value-date']").val(event_time_date.start_date);
                      $(fieldset[1]).find("input[id*='value-time']").val(start_time_hours + ":" + start_time_mins + " " + start_time_suffix);

                      // Insert end time into the page.
                      $(fieldset[1]).find("input[id*='value2-date']").val(event_time_date.end_date);
                      $(fieldset[1]).find("input[id*='value2-time']").val(end_time_hours + ":" + end_time_mins + " " + end_time_suffix);
                    }

                    // Insert title into the page.
                    $title.find('input').val(event_details.data.title);

                    // Insert the description into the event teaser on the page.  Must use the CKEDITOR instances to access the editor.
                    $event_teaser = $container.find("div[id*='field-event-teaser-und-0-value']");
                    var event_teaser_id = $event_teaser[0]['id'].substring(4, $event_teaser[0]['id'].length);
                    CKEDITOR.instances[event_teaser_id].setData(event_details.data.description_raw);

                    // Insert the cost into the page.  If it is Free ensure that a 0 zero value is added.
                    if ($.isNumeric(event_details.data.cost)) {
                      $container.find("input[id*='field-sph-event-cost']").val(event_details.data.cost);
                    }
                    else if (event_details.data.cost !== null && event_details.data.cost.toLowerCase() == "free") {
                      $container.find("input[id*='field-sph-event-cost']").val('0');
                    }
                  });
                });

                // Show title and event details.
                $title.show();
                $(fieldset[1]).show();

                // Break from loop so that we dont hit else case and hide the title and event details.
                return false;
              }
              else {
                // Place null values in date/time, title and cost.
                $title.find('input').val('');
                $(fieldset[1]).find("input[id*='value-date']").val('');
                $(fieldset[1]).find("input[id*='value-time']").val('');
                $(fieldset[1]).find("input[id*='value2-date']").val('');
                $(fieldset[1]).find("input[id*='value2-time']").val('');
                $container.find("input[id*='field-sph-event-cost']").val('');

                // Hide title and event_details.
                $title.hide();
                $(fieldset[1]).hide();

                // Place null into the event teaser on the page.  Must use the CKEDITOR instances to access the editor.
                $event_teaser = $container.find("div[id*='field-event-teaser-und-0-value']");
                var event_teaser_id = $event_teaser[0]['id'].substring(4, $event_teaser[0]['id'].length);
                CKEDITOR.instances[event_teaser_id].setData('');
              }              
            });
          });
        }
      });

      // Add a listener to each input or a Remote Event URL.
      $('.field-name-field-generic-event-url input').live('input', function() {
        $input = $(this);
        var url = $input.val();
        var matches = url.match(/http(.*)\:\/\/(.*)uwaterloo\.ca\/(.*)\/events\/(.*)/);

        // Once a match of https://uwaterloo.ca/<site_url>/event/, load in the data.
        if (matches.length > 0) {
          // Split the URL apary to get data from API
          var url_parts = $input.val().split("/");
          var site = url_parts[3];

          // Get data from API using site_url.
          $.getJSON("https://api.uwaterloo.ca/v2/events/" + site + ".json?key=dem0hash", function(json) {
            // Step through each event
            $.each(json.data, function(i, event){
              // Set teh container to the earliest td and the title to the title div and fieldset to all the fieldsets.
              $container = $input.closest('td');
              $title = $container.find("div[id*='field-generic-event-title']");
              var fieldset = $container.find('fieldset');

              // If the event and the URL entered are the same, then add date/time, title and cost.
              if (event.link == $input.val()) {
                // Make call to API based on the event_id.
                $.getJSON("https://api.uwaterloo.ca/v2/events/" + site + "/" + event.id + ".json?key=dem0hash", function(event_details){
                  // Step through each date/time and add it to the appropriate places on the page.
                  $.each(event_details.data.times, function(j, event_time_date) {
                    // Check if there is a start date.
                    if (event_time_date.start.length > 0) {
                      var start_time_hours, start_time_mins, start_time_suffix;
                      var end_time_hours, end_time_mins, end_time_suffix;
                      var start_time_present = false;
                      var end_time_present = false;

                      // Check if there is a start time.
                      if (event_time_date.start_time.length > 0) {
                        start_time_present = true;

                        // Get hours and minutes.
                        start_time_hours = event_time_date.start_time.substring(0,2);
                        start_time_mins = event_time_date.start_time.substring(3,5);

                        // Convert 24 hour to 12 hour.
                        start_time_suffix = (start_time_hours >= 12)? 'pm' : 'am';
                        start_time_hours = (start_time_hours > 12)? start_time_hours -12 : start_time_hours;
                        start_time_hours = (start_time_hours == '00')? 12 : start_time_hours;
                      }

                      // Check if there is an end time.
                      if (event_time_date.end_time.length > 0) {
                        end_time_present = true;

                        // Get hours and minutes.
                        end_time_hours = event_time_date.end_time.substring(0,2);
                        end_time_mins = event_time_date.end_time.substring(3,5);

                        // Convert 24 hour to 12 hour.
                        end_time_suffix = (end_time_hours >= 12)? 'pm' : 'am';
                        end_time_hours = (end_time_hours > 12)? end_time_hours -12 : end_time_hours;
                        end_time_hours = (end_time_hours == '00')? 12 : end_time_hours;
                      }

                      // Insert start time into the page.
                      $(fieldset[1]).find("input[id*='value-date']").val(event_time_date.start_date);
                      $(fieldset[1]).find("input[id*='value-time']").val(start_time_hours + ":" + start_time_mins + " " + start_time_suffix);

                      // Insert end time into the page.
                      $(fieldset[1]).find("input[id*='value2-date']").val(event_time_date.end_date);
                      $(fieldset[1]).find("input[id*='value2-time']").val(end_time_hours + ":" + end_time_mins + " " + end_time_suffix);
                    }

                    // Insert title into the page.
                    $title.find('input').val(event_details.data.title);

                    // Insert the description into the event teaser on the page.  Must use the CKEDITOR instances to access the editor.
                    $event_teaser = $container.find("div[id*='field-generic-event-teaser-und-0-value']");
                    var event_teaser_id = $event_teaser[0]['id'].substring(4, $event_teaser[0]['id'].length);
                    CKEDITOR.instances[event_teaser_id].setData(event_details.data.description_raw);

                    // Insert the cost into the page.  If it is Free ensure that a 0 zero value is added.
                    if ($.isNumeric(event_details.data.cost)) {
                      $container.find("input[id*='field-sph-event-cost']").val(event_details.data.cost);
                    }
                    else if (event_details.data.cost !== null && event_details.data.cost.toLowerCase() == "free") {
                      $container.find("input[id*='field-sph-event-cost']").val('0');
                    }
                  });
                });

                // Show title and event details.
                $title.show();
                $(fieldset[1]).show();

                // Break from loop so that we dont hit else case and hide the title and event details.
                return false;
              }
              else {
                // Place null values in date/time, title and cost.
                $title.find('input').val('');
                $(fieldset[1]).find("input[id*='value-date']").val('');
                $(fieldset[1]).find("input[id*='value-time']").val('');
                $(fieldset[1]).find("input[id*='value2-date']").val('');
                $(fieldset[1]).find("input[id*='value2-time']").val('');
                $container.find("input[id*='field-generic-event-cost']").val('');

                // Hide title and event_details.
                $title.hide();
                $(fieldset[1]).hide();

                // Place null into the event teaser on the page.  Must use the CKEDITOR instances to access the editor.
                $event_teaser = $container.find("div[id*='field-generic-event-teaser-und-0-value']");
                var event_teaser_id = $event_teaser[0]['id'].substring(4, $event_teaser[0]['id'].length);
                CKEDITOR.instances[event_teaser_id].setData('');
              }              
            });
          });
        }
      });

      //clicking play/pause button
        $('.videoWrapper .controls .video-control').click(function () {
            if($(".videoWrapper video").get(0).paused){
                $(".videoWrapper video").get(0).play();
                $(this).removeClass("play");
                $(this).addClass("pause");
                $(this).attr('aria-label', 'Pause');
            } else {
                $(".videoWrapper video").get(0).pause();
                $(this).removeClass("pause");
                $(this).addClass("play");
                $(this).attr('aria-label', 'Play');
            }
        });

        //hiding description
        $('.videoWrapper .controls .description-control').click(function () {
            $(".videoWrapper .description").slideToggle();
            $(this).toggleClass("down");
            $(this).toggleClass("up");
        });
    }

  };
})(jQuery);
